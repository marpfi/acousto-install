#!/bin/bash
set -v
###############################################################################
# Adding multiverse to repositories ...                                       #
###############################################################################
sudo apt-get install python-software-properties
sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) main universe"
###############################################################################
# Installing dependencies ...                                                 #
###############################################################################
sudo apt-get install make gfortran libconfig8 libconfig8-dev openmpi-bin libblacs-mpi1 libblacs-mpi-dev libscalapack-mpi1 libscalapack-mpi-dev liblapack-dev libatlas3gf-base
###############################################################################
# Create a symbolic link so that acousto finds scalapack ...                  #
###############################################################################
sudo ln -s /usr/lib/libscalapack-openmpi.a /usr/lib/libscalapack.a
###############################################################################
# Downloading acousto ...                                                     #
###############################################################################
wget http://sourceforge.net/projects/acousto/files/acousto/1.5/acousto-1.5.tar.gz/download -O acousto-1.5.tar.gz
tar -xzf acousto-1.5.tar.gz
cd acousto-1.5
###############################################################################
# Configuring and compiling acousto ...                                       #
###############################################################################
./configure --prefix=/usr/local
make
###############################################################################
# Installing acousto ...                                                      #
###############################################################################
sudo make install
